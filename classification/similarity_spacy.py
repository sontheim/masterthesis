import spacy
import pandas as pd
import de_core_news_sm
#import de_core_news_md
import en_core_web_sm
#import spacy_universal_sentence_encoder

#! python -m spacy download de_core_news_sm #small
#python -m spacy download en_core_web_sm


def most_similar(sim_results, num_results):
    sorting = sim_results.sort_values(by='cos_sim', ascending=False)
    top_hits = sorting.head(num_results)
    top_hits_df = pd.DataFrame.reset_index(top_hits)
    top_hits_df.drop(columns=['index'])
    return top_hits_df

def similarity_calc(input_text, language):
    if language == True: #english model
        data_en = pd.read_csv('model/hasoc_en_train.tsv', error_bad_lines=False, encoding='utf-8', sep='\t', header=0)
        train_set = pd.DataFrame(data_en)
        nlp = de_core_news_sm.load()
    else: #german model_
        data_de = pd.read_csv('model/hasoc_de_train.tsv', error_bad_lines=False, encoding='utf-8', sep='\t', header=0)
        train_set = pd.DataFrame(data_de)
        nlp = en_core_web_sm.load()

    print(input_text)
    input_doc = nlp(input_text)
    sim_vec = []

    for index, row in train_set.iterrows():
        tweet_text = row['text']
        tweet_doc = nlp(tweet_text)
        sim = input_doc.similarity(tweet_doc)
        sim_vec.append(sim)

    sim_results = train_set
    sim_results['cos_sim']= sim_vec
    num_results = 9 #number of top hits to display
    ranking = most_similar(sim_results, num_results)

    return ranking
